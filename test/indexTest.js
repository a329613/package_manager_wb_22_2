const sumar = require('../index');
const assert = require('assert');

// Assert = Afirmacion
// 50% test

describe("Probar la suma de dos numeros",()=>{
    //Afirmamos que 5+5 = 10
    it("Cinco mas cinco es 10", ()=>{
        assert.equal(10,sumar(5,5));
    });
    //Afirmamos que 5+7 != 10
    it("Cinco mas siete es 10", ()=>{
        assert.notEqual(10,sumar(7,5));
    });
});

//  .\node_modules\.bin\mocha
